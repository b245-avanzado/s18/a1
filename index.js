// console.log("Hello World")
/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// Codes for Number 1
function getSum(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
}

function getDifference(num3, num4){
	console.log("Displayed difference of " + num3 + " and " + num4);
	console.log(num3 - num4);
}

getSum(5, 15);

getDifference(20, 5);


// Codes for Number2
function returnProduct(num5, num6){
	let multiply = num5 * num6;
	return multiply;
}

function returnQuotient(num7, num8){
	let divide = num7 / num8;
	return divide;
}

let multiplyNumbers = [50, 10];
let product = returnProduct(multiplyNumbers[0], multiplyNumbers[1]);

let divideNumbers = [50, 10];
let quotient = returnQuotient(divideNumbers[0], divideNumbers[1]);

console.log("The product of " + multiplyNumbers[0] + " and " + multiplyNumbers[1]);
console.log(product);

console.log("The quotient of " + divideNumbers[0] + " and " + divideNumbers[1]);
console.log(quotient);


// Codes for Number 3
function returnCircleArea(radius){
	let getCircleArea = 3.1416 * (radius**2);
	return getCircleArea;
}

let rad = 15
let circleArea = returnCircleArea(rad);

console.log("The result of getting the area of a circle with " + rad + " radius:")
console.log(circleArea);

// Codes for Number 4
function returnAverage(num9, num10, num11, num12){
	let getAverage = (num9 + num10 + num11 + num12) / 4;
	return getAverage;
}

let averageNumbers = [20, 40, 60, 80];
let average = returnAverage(averageNumbers[0], averageNumbers[1], averageNumbers[2], averageNumbers[3]);

console.log("The average of " + averageNumbers[0] + ", " + averageNumbers[1] + ", " + averageNumbers[2] + " and " + averageNumbers[3] + ":")
console.log(average);

// Codes for Number 5
function returnIsPassed (num13, num14){
	let grade = num13 / num14 * 100;
	let isPassed = grade > 75;
	return isPassed;
}

let score = [38, 50];
let isPassingScore = returnIsPassed(score[0], score[1]);

console.log("Is " + score[0] + "/" + score[1] + " a passing score?");
console.log(isPassingScore)